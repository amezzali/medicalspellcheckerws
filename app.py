#!flask/bin/python

from flask import Flask, request, jsonify
import utils

# import lexicon dictionary
d = utils.extract_lexicon("nhs_unigram.txt")

app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False	# I want to mantain the order of the output json


@app.route('/list', methods=['GET'])
def return_list():
    if request.method == 'GET': 
        
        return jsonify(utils.return_dist_list())



@app.route('/<string:dist>/<string:word>', methods=['GET'])
def return_f(word, dist):
    if request.method == 'GET': 
        
        # print(utils.return_best(word, d)) 	# for debugging
        return jsonify(utils.return_best(dist, word, d))


if __name__ == '__main__':
    app.run(debug=True)
