# Medical Spell checker

This package allows to spin a Web Service that, given a suspect mispelled word, returns the 5 best correction candidates.

The algorithm is based on [Jaro-Winkler similarity](https://en.wikipedia.org/wiki/Jaro%E2%80%93Winkler_distance) and it relies on a lexicon which is not included in the package itself.

Such a lexicon (renamed as `nhs_unigram.txt`) should be copied in the package folder.

In the `requirement.txt` the list of required packages is listed.

To run the Web Service, simply use:

```
python3 app.py
```
