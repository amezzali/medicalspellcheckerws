from similarity.jarowinkler import JaroWinkler
import re
import operator

def clean_word(word):
    """
    given a string, strips any non-word characters.
    """
    try: 
        return re.compile('\w+').findall(word)[0]
    except:
        return ''


    
def return_dist_list():
    """
    return the list of all available metrics, with the keys to call them in the URL
    """
    
    d = {}
    
    d[ 'JaroWinkler' ] = 'JaroWinkler'
    
    return d
  

def return_best_JW(x, d):
    """
    return the five best candidates for the mispelled workd x, given a lexicon dictionary d.
    The function relies on the Jaro-Winkler similarity.
    """
    l = []
    jarowinler = JaroWinkler()
    
    for w in d.keys():
        l.append((w, round(1-jarowinler.distance(x, w), 4)))	# append new tuple (word, distance from input)
        l.sort(key=operator.itemgetter(1), reverse=True)	# sorting the list
        l = l[:5] 	# keeps only five entries. It's more efficient if the number of words in the dictionary is begger than 5**5
    
    return dict(l)


def return_best(dist, x, d):
    """
    return the five best candidates for the mispelled workd x, given a lexicon dictionary d, based on the distance "dist"
    """
    
    if dist == 'JaroWinkler':
        return return_best_JW(x, d)
    
    else:
        
        return {'MetricUnknown': 0}


def extract_lexicon(file_name):
    """
    given a corpus in file_name, the function extracts a dictionary of "word": frequency.

    the corpus structured as the input file "nhs_unigram.txt", namely a single line with 
    several comma-separated entries such as:

    "word1": freq1, "word2": freq2, ..." 

    where freq1, freq2 are integer.
    """
    
    d = {}
    with open(file_name) as f:
        for line in f:
            for entry in line.split(', '):
                (key, val) = entry.split(': ')
                if clean_word(key) != '':	# filter out empty after-cleaning words
                    d[ clean_word(key) ] = val
                  
    return d
